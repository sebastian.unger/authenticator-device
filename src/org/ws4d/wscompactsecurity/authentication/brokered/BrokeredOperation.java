package org.ws4d.wscompactsecurity.authentication.brokered;

import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.DefaultBrokerOperation;

public class BrokeredOperation extends DefaultBrokerOperation {

	public BrokeredOperation() {
		this(null);
	}

	public BrokeredOperation(DefaultAuthenticationClient client) {
		super(client);
	}
}
