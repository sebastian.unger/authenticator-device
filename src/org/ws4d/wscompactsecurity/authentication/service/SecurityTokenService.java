package org.ws4d.wscompactsecurity.authentication.service;

import java.io.PrintStream;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.Types.SecurityTokenServiceType;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.authenticator.GiantClassDeviceLauncher;
import org.ws4d.wscompactsecurity.authentication.brokered.BrokeredOperation;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH1;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH2;
import org.ws4d.wscompactsecurity.authorization.RequestAuthorizationOperation;

public class SecurityTokenService extends DefaultSecurityTokenService {

	private void repPrint(int count, String print, PrintStream output) {
		for (int i = 0; i < count; i++) {
			output.print(print);
		}
	}

	ServiceAuthenticationCallbacks sacb = new ServiceAuthenticationCallbacks() {
		@Override
		public void populatePin(int pin, QName mechanism) {
			int minsize = mechanism.toString().length();
			int size = minsize + 15;
			GiantClassDeviceLauncher.getStdOut().print("+-");
			repPrint(size, "-", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println("-+");

			GiantClassDeviceLauncher.getStdOut().print("| Got a pin");
			repPrint(size-9, " ", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println(" |");

			GiantClassDeviceLauncher.getStdOut().print("| Mechanism: " + mechanism.toString());
			repPrint(4, " ", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println(" |");

			String sPin = Integer.toString(pin);
			GiantClassDeviceLauncher.getStdOut().print("| PIN: " + sPin);
			repPrint(size - sPin.length() - 5, " ", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println(" |");

			GiantClassDeviceLauncher.getStdOut().print("+-");
			repPrint(size, "-", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println("-+");
		}
		@Override
		public void authenticationResult(EndpointReference ep, boolean success) {

			StringBuilder sb = new StringBuilder();

			sb.append("Authentication with ");
			sb.append(ep.getAddress().toString());
			sb.append(" was" + (success ? " " : " not ") + "successful");

			GiantClassDeviceLauncher.getStdOut().print("+-");
			repPrint(sb.toString().length(), "-", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println("-+");

			GiantClassDeviceLauncher.getStdOut().println("| " + sb.toString() + " |");

			GiantClassDeviceLauncher.getStdOut().print("+-");
			repPrint(sb.toString().length(), "-", GiantClassDeviceLauncher.getStdOut());
			GiantClassDeviceLauncher.getStdOut().println("-+");

		}
	};


	/* deliver to target and only reply without fault if target reply is positive */



	ClientAuthenticationCallbacks pinCallback = new ClientAuthenticationCallbacks() {

		HashMap data = null;

		@Override
		public HashMap receiveAdditionalAuthenticationData() {
			return data;
		}

		@Override
		public int getOOBpinAsInt(QName mechanism) {
			return GiantClassDeviceLauncher.sGetOOBpinAsInt(mechanism);
		}

		@Override
		public void deliverAuthenticationData(SecurityContext ct) {
			AuthenticationEngine.getInstance().getContextDatabase().addContext(ct);
			AuthenticationEngine.getInstance().saveSecurityAssociations();
		}

		@Override
		public void prereportMechanism(QName mechanism) {
			// TODO Auto-generated method stub

		}

		@Override
		public void provideAdditionalAuthenticationData(Object key, Object value) {
			if (data == null)
				data = new HashMap();
			data.put(key, value);
		}
	};

	public SecurityTokenService() {

		super(SecurityTokenServiceType.AUTHENTICATOR | SecurityTokenServiceType.ENDPOINT | SecurityTokenServiceType.SELFHOSTEDAUTHORIZER | SecurityTokenServiceType.SYNCHRONOUSAUTHORIZER , DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		this.addOperation(new ECC_DH1(sacb));
		this.addOperation(new ECC_DH2(sacb));

		DefaultAuthenticationClient client = new DefaultAuthenticationClient();
		client.setAuthenticationCallbacks(pinCallback);

		this.addOperation(new BrokeredOperation(client));

		this.addOperation(new RequestAuthorizationOperation());

	}

}
