package org.ws4d.wscompactsecurity.authentication.authenticator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.sibi.mycli.ICommandWrapper;
import org.sibi.mycli.MyCLI;
import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.dispatch.DefaultDeviceReference;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DiscoveryClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DiscoveryClientCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationRule;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class GiantClassDeviceLauncher {

	static MyCLI cli;
	static HashMap<String, DeviceReference> discoveredEndpoints = new HashMap<String, DeviceReference>();
	static private final int DEFAULT_PORT = 17935;
	static public PrintStream getStdOut() {
		return cli.getStdOut();
	}

	public static String uriAutoComplete(String uri) {
		String furi = null;
		if (uri.length() < 10 || !uri.substring(0,9).equals("urn:uuid:")) {
			furi = "urn:uuid:" + uri;
		} else {
			furi = uri;
		}
		return furi;
	}

	public static int sGetOOBpinAsInt(QName mechanism) {
		cli.println(" ========= ENTER PIN HERE ========= ");
		//FIXME :'(
		cli.println(" == (twice if necessary - sorry) == ");
		int pin;
		try {
			String sPin = cli.getBufferedReader().readLine();
			pin = Integer.parseInt(sPin);
		} catch (IOException e) {
			e.printStackTrace();
			pin = -1;
		}
		return pin;
	}

	public static void main(String[] args) {

		ArrayList<String> DPWSargs = new ArrayList<>();
		boolean runAsDaemon = false;
		boolean connect = false;
		int port = DEFAULT_PORT;
		String host = "localhost";
		String logfile = null;
		String custDBfile = null;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--daemon") || args[i].equals("-d")) {
				runAsDaemon = true;
				System.out.println("Starting as daemon");
			} else if (args[i].equals("--connect") || args[i].equals("-c")) {
				connect = true;
				System.out.println("Starting as connect client");
			} else if (args[i].equals("--port") || args[i].equals("-p")) {
				port = Integer.parseInt(args[++i]);
				System.out.println("Port: " + port);
			} else if (args[i].equals("--host") || args[i].equals("-h")) {
				host = args[++i];
				System.out.println("Host: " + host);
			} else if (args[i].equals("--logfile") || args[i].equals("-l")) {
				logfile = args[++i];
				System.out.println("Logfile: " + logfile);
			} else if (args[i].equals("--database") || args[i].equals("-db")) {
				custDBfile = args[++i];
				System.out.println("Logfile: " + logfile);
			} else if (args[i].equals("--help") || args[i].equals("-h")) {
				System.out.println("Usage: " + args[0] + "--daemon -d --connect -c --port -p --host -h --logfile -l --database -db");
			} else {
				DPWSargs.add(args[i]);
			}
		}

		GiantDevice theDevice = null;
		if (!connect) {
			JMEDSFramework.start(null);

			/* Configure logging */
			Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
			PrintStream logOut = null;
			try {
				logOut = new PrintStream((logfile == null ? "./jmeds.log": logfile));
				Log.setNormalOutput(logOut);
				Log.setErrorOutput(logOut);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				System.err.println("Could not log to log file - defaulting to stdout");
			}

			/* Configure Device Part */
			theDevice = new GiantDevice(custDBfile);
			try {
				theDevice.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		/* starting CLI */
		/* NOTHING BELOW THIS POINT!!! */
		cli = new MyCLI();
		commands();

		if (!connect) {
			cli.injectCommand("discoverendpoints", null);
		}

		try {
			cli.start(true, runAsDaemon, connect, host, port);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("Time to go. G'bye ol' buddy!");
		}
		// // // // NOT HERE // // // //
		/* GRACEFUL SHUTDOWN */
		if (!connect && runAsDaemon) {
			try {
				theDevice.stop();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JMEDSFramework.stop();
			JMEDSFramework.kill();
		}

	}

	/* *********************************** */
	/* register all the fine commands here */
	/* *********************************** */
	private static void commands() {

		/* show trust relationships */
		cli.registerCommand("showtrustrelationships", "Show existing trustrelationships. Args: none", new ICommandWrapper() {
			@Override
			public boolean invoke(Object[] parameters) {
				Iterator contIt = AuthenticationEngine.getInstance().getContextDatabase().getAllContexts().values().iterator();
				while (contIt.hasNext()) {
					SecurityContext sc = (SecurityContext) contIt.next();
					cli.println(sc.toString());
				}
				return true;
			}
		});

		/* discover endpoints */
		cli.registerCommand("discoverendpoints", "Discover Existing Endpoints", new ICommandWrapper() {
			@Override
			public boolean invoke(Object[] parameters) {
				discoverEndpoints();
				return true;
			}
		});

		/* print discovered endpoints' details */
		cli.registerCommand("showendpoints", "Shows discovered Endpoints. Args: none", new ICommandWrapper() {
			@Override
			public boolean invoke(Object[] parameters) {
				showEndpoints();
				return true;
			}
		});

		/* initiate (in)direct authentication with a target */
		cli.registerCommand("directlyauthenticate", "Authenticate directly with target. Use UI-Authenticator if necessary. Args:"
				+ "\n\targ1 uri of target. Maybe pure uuid or uri:uuid:[uuid digits]. First few digits my suffice.", new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {

				int result = directlyAuthenticate(parameters);

				if (result == DefaultAuthenticationClient.ERR_NO_ERROR) {
					return true;
				} else {
					cli.println("directlyAuthenticate() returned error code " + result);
					return false;
				}
			}
		});

		/* delete single or all trust relationships */
		cli.registerCommand("deletetrustrelationships",
				"Remove existing trust relationships from database. Args: ALL | uuid [uuid...]",
				new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {
				return deleteTrustRelationships(parameters);
			}
		});

		cli.registerCommand("addauthorization",
				"Statically adds an authorization. Args: client-uuid subject-uuid [subject-ressource]",
				new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {
				return addStaticAuthorization(parameters);
			}
		});

		cli.registerCommand("deleteauthorizations",
				"Clears the complete Authorization Database",
				new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {
				return deleteAllAuthorizations();
			}
		});

		cli.registerCommand("showsheep",
				"Shows my sheep. Such as Shawn, you know?!", new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {
				org.ws4d.java.structures.HashMap sheep = AuthorizationEngine.getInstance().getSheep();
				if (sheep.size() == 0) {
					cli.println("Ain't not watching any sheep yet!");
					return true;
				}
				Iterator it = sheep.keySet().iterator();
				while (it.hasNext()) {
					String theSheep = (String) it.next();
					String type = (String) sheep.get(theSheep);
					cli.println(theSheep + " (" + type + ")");
				}
				return true;
			}
		});

		cli.registerCommand("showauthorizationrules", "Show AAAAAALL the Authorizations!",
				new ICommandWrapper() {

			@Override
			public boolean invoke(Object[] parameters) {
				org.ws4d.java.structures.ArrayList rules = AuthorizationEngine.getInstance().getAuthorizationDatabase().getAllRules();
				if (rules.size() == 0) {
					cli.println("No rules yet.");
					return true;
				}
				Iterator it = rules.iterator();
				while (it.hasNext()) {
					AuthorizationRule rule = (AuthorizationRule) it.next();
					cli.println("Rule: may " + rule.getClientUri() + " access " + rule.getSubjectUri() + "'s ressource " + rule.getRessource() + "? " + rule.getDecision().toString());
				}
				return true;
			}
		});

	}

	protected static void showEndpoints() {
		for (DeviceReference devRef : discoveredEndpoints.values()) {
			cli.println(((DefaultDeviceReference)devRef).toString());
		}
	}

	private static void discoverEndpoints() {
		DiscoveryClient dc = new DiscoveryClient(new QName(WSSecurityForDevicesConstants.AuthenticationEndpointType, WSSecurityForDevicesConstants.NAMESPACE));
		dc.setCallbacks(new DiscoveryClientCallbacks() {
			@Override
			public void searchTimeOut() {
				cli.println("[MESSAGE] Done with Searching");
			}
			@Override
			public void deviceFound(DeviceReference devRef) {
				cli.println("[MESSAGE] Found " + devRef.getEndpointReference().getAddress().toString());
				discoveredEndpoints.put(devRef.getEndpointReference().getAddress().toString(), devRef);
			}
		});
		dc.start(false, false, false);
	}

	private static int directlyAuthenticate(Object[] parameters) {
		if (parameters == null || parameters.length < 1) {
			cli.println("[ERROR] Need uri of target as arg #1.\n"
					+ "Maybe pure uuid or uri:uuid:[uuid digits].\n"
					+ "First few digits my suffice."
					+ "Samples:\n"
					+ "\turn:uuid:19fc6dc0-e254-11e2-80c3-01cd7c7b068b or\n"
					+ "\turn:uuid:19fc                                 or\n"
					+ "\t19fc6dc0-e254-11e2-80c3-01cd7c7b068b          or\n"
					+ "\t19fc                                          are alle fine.");
			return -1;
		}
		String uri = (String) parameters[0];
		String furi = uriAutoComplete(uri);

		DeviceReference target = null;
		target = discoveredEndpoints.get(furi);

		if (target == null) {
			int furilen = furi.length();
			for (String key : discoveredEndpoints.keySet()) {
				if (key.substring(0, furilen).equals(furi)) {
					target = discoveredEndpoints.get(key);
					break;
				}
			}
		}

		if (target == null) {
			cli.println("[ERROR] Sorry. No way to figure out who or what " + uri + " or " + furi + " could be");
			return -1;
		}

		DefaultAuthenticationClient ac = new DefaultAuthenticationClient();
		ac.setAuthenticationCallbacks(new ClientAuthenticationCallbacks() {
			@Override
			public void prereportMechanism(QName mechanism) {

			}

			@Override
			public int getOOBpinAsInt(QName mechanism) {
				return GiantClassDeviceLauncher.sGetOOBpinAsInt(mechanism);
			}

			@Override
			public void deliverAuthenticationData(SecurityContext ct) {
				AuthenticationEngine.getInstance().getContextDatabase().addContext(ct);
				AuthenticationEngine.getInstance().saveSecurityAssociations();
			}

			@Override
			public org.ws4d.java.structures.HashMap receiveAdditionalAuthenticationData() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void provideAdditionalAuthenticationData(Object key,
					Object data) {
				// TODO Auto-generated method stub

			}
		});

		return ac.authenticate(target, null, true);
	}

	private static boolean deleteTrustRelationships(Object[] parameters) {
		if (parameters == null || parameters.length == 0) {
			cli.println("[ERROR] Need URI(s) of trust relationship(s) to remove or qualifier ALL");
			return false;
		}
		for (Object p : parameters) {
			String param = (String) p;
			if (param.equals("ALL")) {
				AuthenticationEngine.getInstance().removeAllSecurityAssociations();
				cli.println("[MESSAGE] Cleared all security associations");
				break;
			}

			/* autocomplete if necessary */
			String furi = uriAutoComplete(param);

			SecurityContext target = null;
			target = AuthenticationEngine.getInstance().getContextDatabase().getContextById(furi);
			if (target == null) {
				int furilen = furi.length();
				Iterator it = AuthenticationEngine.getInstance().getContextDatabase().getAllContexts().keySet().iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					if (key.substring(0, furilen).equals(furi)) {
						target = AuthenticationEngine.getInstance().getContextDatabase().getContextById(key);
						break;
					}
				}
			}
			if (target == null) {
				cli.println("[ERROR] Sorry. No way to figure out who or what " + param + " or " + furi + " could be. Ignoring.");
			} else {
				cli.println("[MESSAGE] Deleting " + target.getContextReference());
				AuthenticationEngine.getInstance().removeSecurityAssociation(target.getContextReference());
				AuthenticationEngine.getInstance().saveSecurityAssociations();
			}
		}
		return true;
	}

	private static boolean addStaticAuthorization(Object[] parameters) {
		if (parameters == null || parameters.length < 2 || parameters.length > 3) {
			cli.println("[ERROR] Wrong parameters. Usage:\n"
					+ "\taddauthorization client-uuid subject-uuid [subject-ressource]");
			return false;
		}
		String curi = uriAutoComplete((String) parameters[0]);
		String suri = uriAutoComplete((String) parameters[1]);
		String ressource = ((parameters.length == 3) ? (String) parameters[2] : null );

		cli.println("Saving authorization for " + curi + " on " + suri + "'s ressource " + ((ressource==null) ? "[none]" : ressource));

		AuthorizationEngine authzengine = AuthorizationEngine.getInstance();

		authzengine.addAuthorizationRuleByURI(curi, suri, ressource);

		return true;
	}

	private static boolean deleteAllAuthorizations() {
		AuthorizationEngine.getInstance().clearDatabase();
		return true;
	}

}
