package org.ws4d.wscompactsecurity.authentication.authenticator;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticator.DefaultGiantClassDevice;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.PasswordUserCredential;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.service.SecurityTokenService;

public class GiantDevice extends DefaultGiantClassDevice {

	private static String getSpecificUUID() {
		StringBuilder sb = new StringBuilder();

		sb.append("urn:uuid:");

		String variable = null;

		try {
			Enumeration<InetAddress> addresses = NetworkInterface.getByName("wlan0").getInetAddresses();
			for (InetAddress addr : Collections.list(addresses)) {
				if (addr instanceof Inet4Address) {
					variable = String.format("%02x%02x%02x%02x", addr.getAddress()[0], addr.getAddress()[1], addr.getAddress()[2], addr.getAddress()[3]);
				}
			}
		} catch (SocketException e) {
			Log.info("No interface with name wlan0. Defaulting to fixed string");
			variable = "21f209d4";
		} catch (IndexOutOfBoundsException e) {
			Log.info("Mehhh... fucked something up. Will use the alternate prefix...");
			variable = "31f209d4";
		}

		sb.append(variable);

		sb.append("-68fc-4446-b821-c93d71be1031");

		return sb.toString();
	}

	public static final String DEFAULTAUTHDBNAME = "./authenticator.db";
	public static final String DEFAULTAUTHZDBNAME = "./authz.db";

	public GiantDevice() {
		this(DEFAULTAUTHDBNAME);
	}

	public GiantDevice(String dbFile) {
		super();

		AuthenticationEngine.setDefaultOwnerID(getSpecificUUID());

		this.setEndpointReference(new EndpointReference(new URI(AuthenticationEngine.getSafeDefaultOwnerID())));

		this.setPortTypes(new QNameSet(new QName("ElGiganto", WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix)));

		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		ae.setDbName((dbFile != null) ? dbFile : DEFAULTAUTHDBNAME);
		ae.loadSecurityAssociations();

		ae.addSupportedOOBauthenticationMechanism(new QName(WSSecurityForDevicesConstants.PinAuthentication, WSSecurityForDevicesConstants.NAMESPACE));

		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_AES_CBC128)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_RC4)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						null
						)
				);

		AuthorizationEngine aze = AuthorizationEngine.getInstance();
		aze.setDBName(DEFAULTAUTHZDBNAME);
		aze.loadDatabase();
		aze.addUserCredential("password", new PasswordUserCredential("hallo123"));

		this.addService(new SecurityTokenService());

	}

}
