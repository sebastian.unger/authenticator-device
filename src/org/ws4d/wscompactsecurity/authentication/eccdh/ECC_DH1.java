package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultEndpointECC_DH1;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;

public class ECC_DH1 extends DefaultEndpointECC_DH1 {

	public ECC_DH1(ServiceAuthenticationCallbacks sacb) {
		super(sacb);
	}
}
