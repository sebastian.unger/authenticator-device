package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultEndpointECC_DH2;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;

public class ECC_DH2 extends DefaultEndpointECC_DH2 {

	public ECC_DH2 (ServiceAuthenticationCallbacks sacb) {
		super(sacb);
	}
}
